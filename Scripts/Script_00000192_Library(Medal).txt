# -*- encoding: utf-8 -*-
=begin
=実績設定

=end

#==============================================================================
# ■ NWConst::Library
#==============================================================================
module NWConst::Library  
  # メダル獲得時の効果音
  SE_GAIN_MEDAL = RPG::SE.new("Powerup", 90, 140)
  
  # メダル獲得時の効果音ID
  NO_USE_MEDAL = [
    1003,1004,1013,1014,1023,1024,1033,1034,1043,1044,
    1053,1054,1063,1064,1072,1073
  ]
  
  MEDAL_DATA = {
    # 例
#   メダルID => { ※メダルIDは1~9999に収めてください
#      :icon_id => アイコンID,
#      :title => "タイトル",
#      :description => "解説",
#      :priority => :,
#    },
    1 => {
      :icon_id => 193,
      :title => "The rescue of Hans",
      :description => "You saved the kidnapped villager",
      :priority => 1,
    },
    2 => {
      :icon_id => 193,
      :title => "Descent of Goddess Ilias",
      :description => "You have been recognized as an unblessed hero",
      :priority => 2,
    },
    3 => {
      :icon_id => 191,
      :title => "The Monster Lord's slave",
      :description => "Monster Lord Alice added to the party... or perhaps you were added to hers",
      :priority => 3,
    },
    4 => {
      :icon_id => 191,
      :title => "The Goddess's slave",
      :description => "Goddess Ilias added to the party... or perhaps you were added to hers",
      :priority => 4,
    },
    5 => {
      :icon_id => 193,
      :title => "Childhood tag-along",
      :description => "Sonya forced her way into the party...",
      :priority => 5,
    },
    6 => {
      :icon_id => 193,
      :title => "A Proof of Bravery",
      :description => "You've been recognized by a guard for the discovery of the [Proof of Bravery]",
      :priority => 6,
    },
    7 => {
      :icon_id => 193,
      :title => "Merchant rescue",
      :description => "You brought the lost merchant to safety",
      :priority => 7,
    },
    8 => {
      :icon_id => 193,
      :title => "Bandit annihilation",
      :description => "You won against the monster bandits and rehabilitated the girls",
      :priority => 8,
    },
    9 => {
      :icon_id => 193,
      :title => "Resolved Phoenix Tail trade",
      :description => "You liberated the deceived Phoenix Girl",
      :priority => 9,
    },
    10 => {
      :icon_id => 192,
      :title => "Traveler from another world",
      :description => "You continue your adventure in a parallel world",
      :priority => 10,
    },
    11 => {
      :icon_id => 193,
      :title => "Catching harpies",
      :description => "You cured the harpies of the epidemic",
      :priority => 11,
    },
    12 => {
      :icon_id => 193,
      :title => "Mysterious Medal Queen",
      :description => "You received your first audience with the Medal Queen",
      :priority => 12,
    },
    13 => {
      :icon_id => 193,
      :title => "Slug extermination",
      :description => "You triumphed over the slug boss in Slug Tower",
      :priority => 13,
    },
    15 => {
      :icon_id => 192,
      :title => "Chasing your father",
      :description => "One day, you will catch up to him",
      :priority => 15,
    },
    16 => {
      :icon_id => 193,
      :title => "Disappearance of Micaela",
      :description => "Where did she go...",
      :priority => 16,
    },
    17 => {
      :icon_id => 192,
      :title => "Welcome to Sentora",
      :description => "Your first visit to the continent of Sentora",
      :priority => 17,
    },
    18 => {
      :icon_id => 193,
      :title => "Collapse of a shota-harem",
      :description => "You defeated the brainwashed Meia",
      :priority => 18,
    },
    19 => {
      :icon_id => 193,
      :title => "Chasing the Four Spirits",
      :description => "You seek the strength to walk a righteous path from history",
      :priority => 19,
    },
    20 => {
      :icon_id => 192,
      :title => "As a hero...",
      :description => "The last wish of Micaela, engraved upon your heart",
      :priority => 20,
    },
    21 => {
      :icon_id => 193,
      :title => "Mechanized Pope No. 7",
      :description => "You discovered the secret of the church...",
      :priority => 21,
    },
    22 => {
      :icon_id => 193,
      :title => "Haunted mansion curtain call",
      :description => "You put an end to the disturbance at the haunted mansion and delivered punishment to Chrome",
      :priority => 22,
    },
    23 => {
      :icon_id => 192,
      :title => "The spirit of the wind",
      :description => "You acquired the power of Sylph",
      :priority => 23,
    },
    24 => {
      :icon_id => 193,
      :title => "A soul of burning justice",
      :description => "Destined to be a hero after all",
      :priority => 24,
    },
    25 => {
      :icon_id => 193,
      :title => "Black Alice's Counterattack",
      :description => "The devilish tea party never ends",
      :priority => 25,
    },
    26 => {
      :icon_id => 193,
      :title => "Tomboy princess honor restored",
      :description => "Secretly saved Sabasa",
      :priority => 26,
    },
    27 => {
      :icon_id => 193,
      :title => "The idol's curtain call",
      :description => "Saki, flash!☆",
      :priority => 27,
    },
    28 => {
      :icon_id => 193,
      :title => "The witch's dinner",
      :description => "You have settled the troubles in Witch Hunt Village",
      :priority => 28,
    },
    29 => {
      :icon_id => 192,
      :title => "The spirit of earth",
      :description => "You acquired the power of Gnome",
      :priority => 29,
    },
    30 => {
      :icon_id => 191,
      :title => "Wish from a ruined world",
      :description => "You engraved the desires of La Croix upon your heart",
      :priority => 30,
    },
#==============================================================================
    # 買い物金額
    1001 => {
      :icon_id => 193,
      :title => "First time shopper",
      :description => "You bought your first item at a shop",
      :priority => 1001,    
    },
    1002 => {
      :icon_id => 193,
      :title => "Regular shopper",
      :description => "You bought 30,000G worth of goods; you really like your shops",
      :priority => 1002,    
    },
    1003 => {
      :icon_id => 192,
      :title => "Spent 300,000G shopping",
      :description => "You bought 300,000G worth of goods; you're quite the heavy spender",
      :priority => 1003,    
    },
    1004 => {
      :icon_id => 191,
      :title => "Spent 3,000,000G shopping",
      :description => "You bought 3,000,000G worth of goods; is there anything you haven't bought?",
      :priority => 1004,    
    },
    # 鍛冶利用数
    1011 => {
      :icon_id => 193,
      :title => "Your first synthesis",
      :description => "You commissioned your first synthesis to a blacksmith",
      :priority => 1011,    
    },
    1012 => {
      :icon_id => 193,
      :title => "Blacksmith binger",
      :description => "You commissioned 50 blacksmith syntheses; kind of a habit now",
      :priority => 1012,    
    },
    1013 => {
      :icon_id => 192,
      :title => "200 blacksmith syntheses",
      :description => "You commissioned 200 syntheses to a blacksmith; what else can you make?",
      :priority => 1013,    
    },
    1014 => {
      :icon_id => 191,
      :title => "500 blacksmith syntheses",
      :description => "You commissioned 500 syntheses to a blacksmith; you just have to keep going",
      :priority => 1014,    
    },
    # 転職回数
    1021 => {
      :icon_id => 193,
      :title => "Your first job change",
      :description => "You made your first job change in Ilias Temple",
      :priority => 1021,    
    },
    1022 => {
      :icon_id => 193,
      :title => "Job hopper",
      :description => "You changed jobs 10 times; looks bad on your resume",
      :priority => 1022,    
    },
    1023 => {
      :icon_id => 192,
      :title => "100 jobs changed",
      :description => "You changed jobs 100 times; you'll find one you like eventually",
      :priority => 1023,    
    },
    1024 => {
      :icon_id => 191,
      :title => "500 jobs changed",
      :description => "You changed jobs 500 times; maybe you like every job",
      :priority => 1024,    
    },
    # 転種回数
    1031 => {
      :icon_id => 193,
      :title => "Your first race change",
      :description => "You changed races for the first time in Ilias Temple",
      :priority => 1031,    
    },
    1032 => {
      :icon_id => 193,
      :title => "Habitual race changer",
      :description => "You changed races 5 times now; you start to understand different species",
      :priority => 1032,    
    },
    1033 => {
      :icon_id => 192,
      :title => "50 races changed",
      :description => "You changed races 50 times; you're excited to see what's next",
      :priority => 1033,    
    },
    1034 => {
      :icon_id => 191,
      :title => "300 races changed",
      :description => "You changed races 300 times; it's almost like you're playing God",
      :priority => 1034,    
    },
    # 戦闘回数
    1041 => {
      :icon_id => 193,
      :title => "Your first battle!",
      :description => "You fought an opponent for the first time, an unforgettable event",
      :priority => 1041,    
    },
    1042 => {
      :icon_id => 193,
      :title => "Seasoned adventurer",
      :description => "You've made it through 300 battles; you're quite the adventurer",
      :priority => 1042,    
    },
    1043 => {
      :icon_id => 192,
      :title => "1000 fights",
      :description => "You've made it through a thousand fights, and you're ready for a thousand more",
      :priority => 1043,    
    },
    1044 => {
      :icon_id => 191,
      :title => "3000 fights",
      :description => "You've made it through 3000 fights; you're practically unstoppable",
      :priority => 1044,    
    },
    # 逃亡回数
    1051 => {
      :icon_id => 193,
      :title => "Your first escape",
      :description => "You've committed your first act of cowardice by running away",
      :priority => 1051,    
    },
    1052 => {
      :icon_id => 193,
      :title => "Keep running",
      :description => "You've escaped 10 times; running away just feels natural at this point",
      :priority => 1052,    
    },
    1053 => {
      :icon_id => 192,
      :title => "50 escapes",
      :description => "You ran away 50 times; you're getting used to leaving your enemies in the dust",
      :priority => 1053,    
    },
    1054 => {
      :icon_id => 191,
      :title => "150 escapes",
      :description => "You ran away 150 times; you tell yourself fighting isn't always the answer",
      :priority => 1054,    
    },
    # 敗北回数
    1061 => {
      :icon_id => 193,
      :title => "Your first defeat",
      :description => "You have been defeated by your opponent, receiving your rewar- No, your disgrace",
      :priority => 1061,    
    },
    1062 => {
      :icon_id => 193,
      :title => "Adventurer bait",
      :description => "You have been defeated 30 times; that just makes you prey",
      :priority => 1062,    
    },
    1063 => {
      :icon_id => 192,
      :title => "100 defeats",
      :description => "You have been defeated 100 times, hopefully not on purpose",
      :priority => 1063,    
    },
    1064 => {
      :icon_id => 191,
      :title => "500 defeats",
      :description => "You have been defeated 500 times, psychological damage be damned",
      :priority => 1064,    
    },
    # 総撃破数
    1071 => {
      :icon_id => 193,
      :title => "Monster killer",
      :description => "You've turned the tables on 500 monsters",
      :priority => 1071,    
    },
    1072 => {
      :icon_id => 192,
      :title => "3000 opponents crushed",
      :description => "You've crushed 3000 opponents; you get stronger with each one that falls",
      :priority => 1072,    
    },
    1073 => {
      :icon_id => 191,
      :title => "10000 opponents crushed",
      :description => "You've crushed 10000 opponents; too easy",
      :priority => 1073,    
    },
    # 職業公開率
    1201 => {
      :icon_id => 191,
      :title => "Jobs mastered: 100%",
      :description => "You've mastered every job",
      :priority => 1201,    
    },
    # 種族公開率
    1202 => {
      :icon_id => 191,
      :title => "Races mastered: 100%",
      :description => "You've mastered every race",
      :priority => 1202,    
    },
    # キャラ図鑑コンプ率
    1301 => {
      :icon_id => 191,
      :title => "Character book: 100%",
      :description => "You've filled out all entries in the character book",
      :priority => 1301,    
    },
    # 魔物図鑑コンプ率
    1311 => {
      :icon_id => 191,
      :title => "Monster book: 100%",
      :description => "You've filled out all entries in the monster book",
      :priority => 1311,    
    },
    # 武器図鑑コンプ率
    1321 => {
      :icon_id => 191,
      :title => "Weapon book: 100%",
      :description => "You've filled out all entries in the weapon book",
      :priority => 1321,    
    },
    # 防具図鑑コンプ率
    1331 => {
      :icon_id => 191,
      :title => "Armor book: 100%",
      :description => "You've filled out all entries in the armor book",
      :priority => 1331,    
    },
    # アクセサリ図鑑コンプ率
    1341 => {
      :icon_id => 191,
      :title => "Accessory book: 100%",
      :description => "You've filled out all entries in the accessory book",
      :priority => 1341,    
    },
    # アイテム図鑑コンプ率
    1351 => {
      :icon_id => 191,
      :title => "Item book: 100%",
      :description => "You've filled out all entries in the item book",
      :priority => 1351,    
    },
#==============================================================================
    1401 => {
      :icon_id => 193,
      :title => "Medal beginner",
      :description => "You've started a small medal collection",
      :priority => 1401,    
    },
    1402 => {
      :icon_id => 193,
      :title => "Medal collector",
      :description => "You learned the name of the Medal Queen",
      :priority => 1402,    
    },
    1411 => {
      :icon_id => 193,
      :title => "First BF victory",
      :description => "You got your first victory in a Battle Fuck",
      :priority => 1411,    
    },
    1412 => {
      :icon_id => 193,
      :title => "Accomplished Battle Fucker",
      :description => "You defeated 15 Battle Fuckers",
      :priority => 1412,    
    },
    1421 => {
      :icon_id => 193,
      :title => "First BF defeat",
      :description => "You received your first defeat in a Battle Fuck",
      :priority => 1421,    
    },
    1422 => {
      :icon_id => 193,
      :title => "Shameful loser",
      :description => "You have lost 30 Battle Fucks",
      :priority => 1422,    
    },
    1501 => {
      :icon_id => 193,
      :title => "Little shop of horas",
      :description => "The beginning of the item shop: dealing herb",
      :priority => 1501,    
    },
    1502 => {
      :icon_id => 193,
      :title => "A nice shop",
      :description => "A satisfying personal item shop",
      :priority => 1502,    
    },
    1503 => {
      :icon_id => 192,
      :title => "A fully-stocked shop",
      :description => "Displaying all the goods Sentora continent has to offer",
      :priority => 1503,    
    },
    1511 => {
      :icon_id => 193,
      :title => "Papi the blacksmith",
      :description => "A good first step towards being a master blacksmith",
      :priority => 1511,    
    },
    1512 => {
      :icon_id => 192,
      :title => "First class blacksmith",
      :description => "You even get clients coming from the other side of the world",
      :priority => 1512,    
    },
#==============================================================================
    2000 => {
      :icon_id => 193,
      :title => "Freedom",
      :description => "Adventure is a matter of liberty",
      :priority => 2000,
    },
    2001 => {
      :icon_id => 192,
      :title => "Strike freedom",
      :description => "Luka, loyal to himself",
      :priority => 2001,
    },
    2005 => {
      :icon_id => 192,
      :title => "A desperate Nero",
      :description => "The ordinary traveler seems to have royally screwed up",
      :priority => 2005,
    },
    2006 => {
      :icon_id => 193,
      :title => "The murder of Amira",
      :description => "She'll just come back as if nothing even happened",
      :priority => 2006,
    },
    2007 => {
      :icon_id => 191,
      :title => "Bourgeoisie",
      :description => "You learn how the other half lives while staying at an ultra-luxurious inn",
      :priority => 2007,
    },
    2008 => {
      :icon_id => 193,
      :title => "The bats of Witch Hunt",
      :description => "You kept your eyes peeled",
      :priority => 2008,
    },
    2009 => {
      :icon_id => 193,
      :title => "Enlightened Dog Girl",
      :description => "Although she's enlighted, she still loves meaty bones",
      :priority => 2009,
    },
    2010 => {
      :icon_id => 193,
      :title => "Sheep's world tour",
      :description => "Spot the drunk sheep in bars around the world",
      :priority => 2010,
    },
    2011 => {
      :icon_id => 193,
      :title => "Best friends",
      :description => "Nuruko and Gnome feel a bond joining them...",
      :priority => 2011,
    },
    2012 => {
      :icon_id => 191,
      :title => "Slot master",
      :description => "You got 777 in the slot machine",
      :priority => 2012,
    },
    2013 => {
      :icon_id => 191,
      :title => "In-battle 777",
      :description => "You got 777 while fighting",
      :priority => 2013,
    },
    2014 => {
      :icon_id => 191,
      :title => "Poker master",
      :description => "You were dealt a Royal Flush while playing poker",
      :priority => 2014,
    },
    2015 => {
      :icon_id => 191,
      :title => "In-battle Royal Flush",
      :description => "You were dealt a Royal Flush while fighting",
      :priority => 2015,
    },
    2016 => {
      :icon_id => 193,
      :title => "Universe in conflict",
      :description => "You will see the tears of the stars",
      :priority => 2016,
    },
    2017 => {
      :icon_id => 193,
      :title => "Gravity-defying imp",
      :description => "We heard rumours about a suicidal imp...",
      :priority => 2017,
    },
    2018 => {
      :icon_id => 193,
      :title => "Chi pa pa",
      :description => "The flower on your head goes chi pa pa",
      :priority => 2018,
    },
    2020 => {
      :icon_id => 193,
      :title => "Find me...",
      :description => "You found Amira hiding in a town",
      :priority => 2020,
    },
    2021 => {
      :icon_id => 192,
      :title => "The 15th Amira",
      :description => "You found Amira hiding in a town 15 times",
      :priority => 2021,
    },
    2031 => {
      :icon_id => 193,
      :title => "The first sniff",
      :description => "You showed your first panty to Panty Sensei",
      :priority => 2031,
    },
    2032 => {
      :icon_id => 192,
      :title => "Panty collector",
      :description => "You showed 50 panties to Panty Sensei",
      :priority => 2032,
    },
    2039 => {
      :icon_id => 191,
      :title => "The death of Panty Sensei",
      :description => "All the perverts of the world cry over his disappearance",
      :priority => 2039,
    },

#==============================================================================
    3001 => {
      :icon_id => 191,
      :title => "Angel, kill! Kill!",
      :description => "You defeated the armored berserker",
      :priority => 3001,
    },
    3002 => {
      :icon_id => 192,
      :title => "Seventh Heaven",
      :description => "You defeated Nanabi",
      :priority => 3002,
    },
    3003 => {
      :icon_id => 192,
      :title => "I hate riddles",
      :description => "You defeated Sphinx",
      :priority => 3003,
    },
    3004 => {
      :icon_id => 192,
      :title => "Monster Lord's defeat?",
      :description => "You defeated Hades Alice",
      :priority => 3004,
    },
    3005 => {
      :icon_id => 191,
      :title => "Conquered death itself",
      :description => "You defeated Reaper",
      :priority => 3005,
    },
    3100 => {
      :icon_id => 193,
      :title => "Labyrinth challenger",
      :description => "You reached the 10th level of the Labyrinth of Chaos",
      :priority => 3100,
    },
    3101 => {
      :icon_id => 192,
      :title => "Labyrinth expert",
      :description => "You reached the 50th level of the Labyrinth of Chaos",
      :priority => 3101,
    },
    3102 => {
      :icon_id => 191,
      :title => "Labyrinth master",
      :description => "You reached the 100th level of the Labyrinth of Chaos",
      :priority => 3102,
    },
    3103 => {
      :icon_id => 191,
      :title => "Labyrinth God",
      :description => "You reached the 200th level of the Labyrinth of Chaos",
      :priority => 3103,
    },
  }
end